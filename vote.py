async def voting(ctx):
    global vote, title, user_ids, submit_user_ids, setup_message
    # print (user_ids)
    try:
        if setup_message is not None:
            try:
                number = int(ctx.content[2:])
                if number > len(vote) or number < 1:
                    await discordbot.delete_send(ctx.channel, content="沒有" + str(number) + "這個號碼ㄛ")
                else:
                    user_ids.append(ctx.author.id)
                    vote[number - 1] += 1
                    await discordbot.reload_vote_msg()
                    await discordbot.delete_send(ctx.channel, content="豪 " + str(number) + "+1")

            except:
                if len(vote) < 10:
                    new_title = ctx.content[2:]
                    while new_title.startswith(' '):
                        new_title = new_title[1:]
                    if new_title in title:
                        await discordbot.delete_send(ctx.channel, content="可以乖乖用號碼投票ㄇ")
                    else:
                        vote.append(1)
                        title.append(new_title)
                        submit_user_ids.append(ctx.author.id)
                        user_ids.append(ctx.author.id)
                        await discordbot.reload_vote_msg()
                        await discordbot.delete_send(ctx.channel, content="增加新選項：**" + new_title + "**")
                        print('新選項：' + str(len(vote)) + '. ' + new_title + ' by ' + str(ctx.author))
                else:
                    await discordbot.delete_send(ctx.channel, content="沒新選項ㄉ空間ㄌ")
                    # print ('Too many options! '+ str(len(vote)))




        else:
            await discordbot.delete_send(ctx.channel, content="現在還不能投票ㄋ")
            # print (ctx.author.display_name+' has already voted!')
    except (NameError):
        pass


@disbot.command(pass_context=True)
async def clvote(ctx):
    await disbot.delete_message(ctx.message)
    global user_ids
    user_ids = []